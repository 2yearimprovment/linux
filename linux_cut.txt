cut -d delimiter  -f number file

Delimiter is the delimiter character 
anything before the delimiter and after it is considered separate fields. Number is the number of the field.

example

cut -d : -f1 /etc/passwd